﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaunchCAF_List.aspx.cs" Inherits="RM.Web.RMBase.CAF.Launch_CAF" %>

<%@ Register Src="../../UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    
        <script type="text/javascript">
        $(function() {
            divresize(63);
            FixedTableHeader("#table1", $(window).height() - 91);
        });
        function add() {
            var url = "/RMBase/CAF/LaunchCAF_Form.aspx";
            //top.openDialog(url, 'LaunchCAF_Form', 'CAF审批 -发起', 450, 325, 50, 50);
            top.open(url);
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">


        <div class="btnbarcontetn">
            <div style="text-align:left;">
                <uc1:LoadButton ID="LoadButton1" runat="server" />
            </div>
        </div>

        <div class="btnbartitle">
            <div>
                代办事项
            </div>
        </div>
        <div class="btnbartitle">
            <div>
                已办事项
            </div>
        </div>

    </form>
</body>
</html>
